//
//  ViewController.swift
//  NotifyWithLogin
//
//  Created by User on 26/12/19.
//  Copyright © 2019 aditya. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var idTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
   var login = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func loginActionClicked(_ sender: Any) {
        
        Alamofire.request("https://reqres.in/api/login", method: .post, parameters: ["email":idTextField.text!, "password": passwordTextField.text!], encoding: JSONEncoding.default, headers: ["Content-Type": "application/json"]).validate().responseJSON(completionHandler: ({response in
            
            let dict = response.value as? [String : Any]
            
            let str = dict!["token"] as! String
            
            self.login = true
            
            UserDefaults.standard.set(self.login, forKey: "isLogin")//set(self.login, forKey: "isLogin")
            UserDefaults.standard.set(str, forKey: "token")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "NextViewController")
            
            self.present(vc!, animated: true, completion: nil)
           
        }))
    }
    
}

