//
//  NextViewController.swift
//  NotifyWithLogin
//
//  Created by User on 26/12/19.
//  Copyright © 2019 aditya. All rights reserved.
//

import UIKit
import WebKit

class NextViewController: UIViewController,WKNavigationDelegate {

   
    @IBOutlet weak var responseWebView: WKWebView!
    
    override func loadView() {
        responseWebView = WKWebView()
        responseWebView.navigationDelegate = self
        view = responseWebView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        var str = "https://mckinleyrice.com/?token=" + (UserDefaults.standard.string(forKey: "token")!)
        
        let url = URL(string: str)
        responseWebView.load(URLRequest(url: url!))
        responseWebView.allowsBackForwardNavigationGestures = true
    }

}
